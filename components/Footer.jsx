import React from 'react';
import { AiFillLinkedin } from 'react-icons/ai';
import { FaGithubSquare } from 'react-icons/fa';
import { BiWorld } from 'react-icons/bi';

const Footer = () => {
  return (
    <div className="footer-container">
      <p>2022 YAM LITTLE ECOMMERCE All rights reserverd</p>
      <p className="icons">
        <a href="https://www.linkedin.com/in/yamil-pedroso/" target="_blank">
          <AiFillLinkedin />
        </a>
        <a href="https://github.com/Yamil-Pedroso" target="_blank">
          <FaGithubSquare />
        </a>
        <a href="https://www.yamilwebdeveloper.com/" target="_blank">
          <BiWorld />
        </a>
      </p>
    </div>
  )
}

export default Footer
